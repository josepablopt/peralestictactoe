# Tic-Tac-Toe

Implement a Tic-Tac-Toe game using LISP.
 Build from the bottom up, commenting each of your functions as you create them. Begin with the functions built in class.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
need to know a basic understanding of LISP 
```

### Installing

A step by step series of examples that tell you how to get a development env running


## Running the tests

No Test yet

### Break down into end to end tests




## Built With

* [Common LISP] 

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.



## Authors

* **Jose Perales** - 


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Cassandra Enriquez 

