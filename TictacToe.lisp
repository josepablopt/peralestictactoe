;;;; tictactoe.lisp
;;;; Perales Jose


;;Printing the board from class:
;;Param: board - a list containing elements of a ttt board in row major order 


(defun print-board (board)
	(format t "=============")
		(do ((i 0 (+i 1)))
			((= i 9) nill)
			(if (= (mod i 3) 0)
				(format t "~%|")
				nill)
			(format t " ~A |" (nth i board))
		)
		(format t "~%=============")
)


;; Testing if the move is legal or not.

(defun tic-legal)	


;; these are the ways that a user or a computer can win:

(defparameter *typesofwins* '((1 2 3) (4 5 6) (7 8 9) (1 4 7) (2 5 8) (3 6 9) (1 5 9) (3 5 7)))
(defparameter *current-player* 'x)) 

;;getting an element from the board

(defun get-board-elt (n board)
	(nth (1-n) board))

;;checking to see if the user has won. Passing in the board. 

(defun win-types (board)
	(loop for i in *typesofwins* 
		collect *(loop for j from 0 to 2 
			collect (get-board-elt(nth j i ) board))))
			

;;checking to see if the move was legal

(defun legal-pass (n board)
	(null (get-board-elt n board)))


